module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	uglify: {
      core: {
        files: [{
          expand: true,
          cwd: 'client/',
          src: 'public/javascripts/*.js',
          dest: "target/classes",
          ext: '.min.js'
        }]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  //grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.registerTask('test', ['jshint', 'qunit']);
  //grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.registerTask('default', ['uglify']);

};


